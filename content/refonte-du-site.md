---
Title: Refonte du site avec PicoCMS
Date: 2020-12-31
Author: Phenix
Description:
---
Depuis pas mal de temps, je veux refaire ce site (encore !)

Oui, il y a eu pas mal de version, de refonte, pas toujours justifiée en fonction de mes humeurs.

En gros, la toute première version était mon travail de fin d'étude sur base d'un CMS maison (FlameCMS xD) et animé en javascript.
Ensuite une version Wordpress qui n'a pas tenu très longtemps, parce que développer mon propre CMS dans mon coin, c'est quand même vachement difficile.
Puis est venu la version [SPIP](https://www.spip.net/) c'était le logiciel qui j'utilisais de manière professionnel. Avoir mon blog sous SPIP me permettait d'expérimenter.

Aujourd'hui mes besoins sont différents. Premièrement je veux que ce site soit plus rapide et plus léger. Je le veux aussi statique que possible et en markdown. Je veux les update via Git et un système de déployement.
Mes besoins ont évolué, ce site a toujours été ma documentation personnel, mais j'ai besoin de pouvoir rapidement mettre en place des articles depuis [Spacemacs](https://spacemacs.org "Spacemacs website") à tout moment sans que cela ne soit un soucis.
J'ai aussi besoin de mettre de l'ordre dans tout ces articles qui sont en ligne depuis une éternité et qui mérite très certainement la poubelle aujourd'hui.

Cet article ce veux être une explication sur comment je m'y suis pris et les outils que j'ai utlisé.

Pour commencé, installons [PicoCMS](https://picocms.org/ "PicoCMS site officiel"):

```Bash
composer create-project picocms/pico-composer pico
```

Je publie pas mal de code. Un des reproche que je peux faire a SPIP, c'est qu'il s'occupe de la coloration syntaxique du code coté en PHP, via une très ancienne librairie: Geshi. A l'heure ou j'écris ces lignes, le site officiel de la librairie renvoie des erreurs SQL `Warning: mysqli_error()`.

Ici, je vais simplement utiliser [highlight.js](https://highlightjs.org "highlight.js Official website") avec le thème monokai :)

Voici ce que cela donne:

```php
<?php // @codingStandardsIgnoreFile
/**
 * This file is part of Pico. It's copyrighted by the contributors recorded
 * in the version control history of the file, available from the following
 * original location
:
 *
 * <https://github.com/picocms/pico-composer/blob/master/index.php>
 *
 * SPDX-License-Identifier: MIT
 * License-Filename: LICENSE
 */

// load dependencies
// pico-composer MUST be installed as root package
if (is_file(__DIR__ . '/vendor/autoload.php')) {
    require_once(__DIR__ . '/vendor/autoload.php');
} else {
    die("Cannot find 'vendor/autoload.php'. Run `composer install`.");
}

// instance Pico
$pico = new Pico(
    __DIR__,    // root dir
    'config/',  // config dir
    'plugins/', // plugins dir
    'themes/'   // themes dir
);

// override configuration?
//$pico->setConfig(array());

// run application
echo $pico->run();
```

C'est déjà cela de fait !
Maintenant, je dois organiser mon blog en catégorie. La page principal doit également afficher les 5 derniers articles.

Pico n'est pas vraiment un logiciel dédier à faire des blogs. Il est plus général et il faut un peu de retroucer les manches pour y arriver.

Il va nous falloir le plugin de pagination, afficher tout les articles du blog sur une seul page me semble peu efficace. Ce n'est pas supporté par défaut dans pico, il fait ajouter un [plugin](https://github.com/rewdy/Pico-Pagination). Il est listé sur la site officiel, donc j'imagine que c'est la référence en matière de pagination.

Dans un premier temps, on créer un template qui servira a lister les contenu du blog:

```twig
{% for page in paged_pages if not page.hidden %}
<article>
    <h3><a href="{{ page.url }}">{{ page.title }}</a></h3>
    <p class="date">{{ page.date_formatted }}</p>
    <p class="content">{{ page.id|content }}</p>
    </article>
{% endfor %}
```

Remarquez le `{{ page.id|content }}` qui va afficher le contenu de l'article. Je ne suis pas fan de faire attendre les gens, ils peuvent lire sur la home page plutôt que d'avoir a naviguer.

Ensuite je place un `index.md` dans le répertoires de `content`, cela permet d'avoir un index des articles et la pagination avec le plugin.

```yaml
---
hidden: true
Template: blog-index
---
```

Ensuite, pour organiser le contenu, je vais utilier le plugin de tag et des sous dossier.
Les sous-dossier permette de creer les url et les tags d'organiser le contenu. Le fichier index des sous dossier contiendra une option "filter" avec le nom du tag qui correspondra au sous-dossier.
Le fait d'avoir des tags permet de facto de creer une liste pour facilité la navigation dans les différentes catégorie du site.
