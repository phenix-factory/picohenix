---
Title: Get PhpUnit code coverage back
Date: 2021-03-14
Tags: webdev
Lang: en
---
Since I upgraded to laravel 8, my usual phpunit setup greated me with a

```
PHPUnit 9.5.2 by Sebastian Bergmann and contributors.

  Warning - The configuration file did not pass validation!
  The following problems have been detected:

  Line 35:
  - Element 'log': This element is not expected.

  Test results may not be as expected.
```

I m used to have a beautiful little html web site that show me the code coverage with:

```xml
<logging>
    <log type="coverage-html" target="tests/code-coverage/" lowUpperBound="35" highLowerBound="70" />
</logging>
```

But it doesn't work anymore, you need to use the `<coverage>` node:

```xml
<coverage processUncoveredFiles="true">
    <include>
        <directory suffix=".php">./app</directory>
    </include>
    <report>
        <html outputDirectory="html-coverage" lowUpperBound="50" highLowerBound="90"/>
    </report>
</coverage>
```
