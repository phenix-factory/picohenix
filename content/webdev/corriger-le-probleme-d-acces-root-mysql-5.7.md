---
title: Corriger le problème d'acces root sous mysql 5.7
date: 2019-06-23
autgor: Phenix
lang: fr
---
Lorsque l'on installe mysql 5.7, l'utilisateur root ne peux pas ce connecter à la machine. C'est normal bien entendu.

<div class="message is-warning">
<div class="message-header">
Warning
</div>
<div class="message-body">
Ne faite jamais cela sur une machine de production !
</div>
</div>

On peux cependant corriger cela :

Pour ce connecter à Mysql :
`sudo mysql -u root`

Ensuite, on utilise autorise root a ce connecter :

```sql
USE mysql;
UPDATE user SET plugin='mysql_native_password' WHERE User='root';
SET PASSWORD FOR root@localhost=PASSWORD('root');
GRANT ALL PRIVILEGES ON *.* TO root@localhost IDENTIFIED BY 'root' WITH GRANT OPTION;
flush privileges;
```
