---
title: Gitlab, Composer et Authentification
date: 2019-09-03
author: Phenix
lang: fr
---
Vous pouvez passer une variable `COMPOSER_AUTH` en secret dans un votre Gitlab pour vous authentifier !
Il suffit d'y mettre directement le JSON.

La doc est là : [https://getcomposer.org/doc/03-cli.md#composer-auth](https://getcomposer.org/doc/03-cli.md#composer-auth)
