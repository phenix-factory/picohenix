---
Title: Utiliser la session Laravel avec les route/api
Author: Phenix
Date: 2019-12-17
Tags: webdev
---


Je cherchais une solution pour utiliser les routes `api` de Laravel avec la session `web` classique.
Installer Passport est complexe et souvent inutile pour pas mal de projet. Il existe la solution de passer par un champ `api_token` dans la base de donnée, mais niveau sécurité, je trouve cela moyen.

Cependant, il est possible d’utiliser le driver de session classique :

Dans `config/auth.php`, il faut modifier les `guards` pour replacer token par session en dessous d’API :

```php
'guards' => [
    'web' => [
        'driver' => 'session',
        'provider' => 'users',
    ],
    'api' => [
        'driver' => 'session',
        'provider' => 'users',
        'hash' => false,
    ],
],

```

Ensuite dans app/Http/Kernel.php, il faut modifier le middlewareGroups « api » :

```php
'api' => [
    'throttle:60,1',
    \App\Http\Middleware\EncryptCookies::class,
    \Illuminate\Session\Middleware\StartSession::class,
    'bindings',
],
```

Tester avec un projet Laravel 6 sans soucis !
