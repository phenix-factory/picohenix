---
Title: L’héritage important
Author: Phenix
Date: 2020-04-01
Tags: syntaxe-horror
Lang: fr
---

```css
.content {
    height: inherit !important;
}
```

Je ne sais pas à quoi cela peut bien servir. Mais c’est important !
