---
Title: Dymo Label Writer 450 sous Linux Mint 19.2
date: 2019-10-13
author: Phenix
Tags: trucs-et-astuces
lang: fr
---
Il y a un paquet Ubuntu [printer-driver-dymo](apt://printer-driver-dymo).

Voir: https://doc.ubuntu-fr.org/dymo

Une fois le driver installer, CPUS semble voir correctement la Dymo et permet même de choisir le format d'étiquette.
L’imprimante est correctement listée.

![Dymo inside the printer menu](assets/dymo-inside-printer.png)

Par contre impossible d'imprimer depuis un logiciel classique.
L'astuce est d'installer [gLabels](apt://glabels), de sélectionner le format et cela imprimera !

![glables selection de produit](asstes/glables-selection-produit.png)

Il est aussi possible de partager la Dymo en réseau. J'utilise gLabels sur mon portable pour imprimer sur la Dymo de ma tour.

Attention à bien cocher la case "publier les imprimantes" dans les paramètres de Linux Mint :

![share printer menu](assets/share-printer.png)

Sous Windows, le logiciel de Dymo (DYMO Label) pour faire des étiquettes ne semble pas fonctionner avec cette configuration.
