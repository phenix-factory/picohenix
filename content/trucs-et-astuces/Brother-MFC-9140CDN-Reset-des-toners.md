---
Title: Brother MFC 9140CDN - Reset des toners
Author: Phenix
Date: 2020-04-17
Tags: trucs-et-astuces
Lang: fr
---
J’ai une Brother MFC 9140CDN. Chouette imprimante dont je suis très content.
Cependant, comme tous les fabricants d’imprimante, Brother ne peut pas s’empêcher d’arnaquer ces clients en leur vendant :

* Des toners hors de prix
* Des toners qui sont annoncés vide par la machine alors qu’en fait, ils ne le sont pas.

Pour déclencher l’alerte « nouveau toner », il se contente de compter le nombre de page imprimé. Et il a la main lourde sur le calcule, parce que perso, cela fait déjà 2 fois que je remet a 0 les toners sans les changer et ça imprime toujours correctement.

### Pour remettre a 0 (reset) les toners de votre imprimante :

Repérer l’astérix du pavé numérique. On peut allumer le pavé numérique en cliquant sur « fax ».
Ensuite il faut ouvrir le compartiment des toners.
Appuyez pendant 5 secondes sur l’astérix (le compartiment à toner dois rester ouvert).

Un menu apparaît :

* K = noir
* Y = Jaune
* M = Rouge
* C = Blue

Les cartouches qui termine par STD sont cartouche de taille « standard » et les HC sont les grand capacité (HC = Hight capacity).

Le menu est parfois un peu buggé/capricieux, il faut insister sur les boutons.
